Integration of Stripe payment processor.
##################################

Install requirements
^^^^^^^^^^^^^^^^^^^^^

Uses Python 3.4.3 and up.

Install OS requirements

.. code:: python

    sudo apt install libxml2-dev libxmlsec1-dev libxmlsec1-openssl default-libmysqlclient-dev


To install requirements use the following command:

.. code:: python

    pip install -r requirements.txt --find-links=libraries


Create your environment file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Extra settings file are loaded like this

.. code:: python

    exec('from .host_settings.{} import *'.format(HOST_NAME))

So you need to create a file on 'ivr_project.host_settings.<your host name>', you
can use ``hostname`` command to know your hostname

**Suggestion:** Copy the content of ivr_project/host_settings/example.pu on
core_payments/host_settings/<your host name>.py

Docker database
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you haven't a mysql database installation you could use a docker image
to create phpmyadmin installation faster with docker.

.. code:: python

    docker run --name myadmin -d -e UPLOAD_LIMIT=30M -e PMA_HOST=172.17.0.1 -e
    PMA_PORT=3306 -p 8090:80 phpmyadmin


Run the project
^^^^^^^^^^^^^^^^^^^^^
Just run this code on ivr_project

.. code:: python

    python manage.py runserver 127.0.0.1:8812

Send request to the processor api
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Api endpoint

.. code:: python

    <your-host>/coreservices/payments/api/payments/transactions/<project-id>/

Note: **project-id** value could be any number

Send all require data through request's body.

**Suggestion:** This is a Rest integration, so the request data format should be in JSON.

If you want to send your request through `Postman` we provided you
a collection file in the `/postman_collection` directory.

Also, you can find below a couple of examples of request data:

ACH Transaction

.. code:: Python

    {
    "processor": "stripe",
    "mode":"test",
    "credentials":{
            "api_access_key":"your stripe api key"
                },
    "interaction_id": "12345678901",
    "interaction_type":"web",
    "transaction_type":"sale",
    "tender_type":"ach",
    "ach_name_on_account": "John Doe",
    "ach_account_number": "000123456789",
    "ach_routing_number": "110000000",
    "ach_account_type": "individual",
    "amount": "50",
    "bill_to_first_name": "John",
    "bill_to_last_name": "Doe",
    "bill_to_email": "john.doe@ivrtechnologym.com",
    "bill_to_company": "IVR Technology",
    "bill_to_address": "65 Lawrence Bell Drive",
    "bill_to_city": "Amherst",
    "bill_to_state": "New York",
    "bill_to_zip": "14221",
    "bill_to_country": "United States",
    "ship_to_first_name": "John",
    "ship_to_last_name": "Doe",
    "ship_to_email": "john.doe@ivrtechnology.com",
    "ship_to_company": "IVR Technology",
    "ship_to_address": "65 Lawrence Bell Drive",
    "ship_to_city": "Amherst",
    "ship_to_state": "New York",
    "ship_to_zip": "14221",
    "ship_to_country": "United States",
    "description": "Sale Testing",
    "invoice_id": "00000123"
}


CC Transaction

.. code:: Python

    {
    "processor": "stripe",
    "mode":"test",
    "credentials":{
            "api_access_key":"your stripe api key"
                },
    "interaction_id": "12345678901",
    "interaction_type":"web",
    "transaction_type":"sale",
    "tender_type":"ach",
    "tender_type":"credit_card",
    "card_account_number": "4242424242424242",
    "card_verification_value": "314",
    "card_expiry_month": "05",
    "card_expiry_year": "21",
    "amount": "50",
    "bill_to_first_name": "John",
    "bill_to_last_name": "Doe",
    "bill_to_email": "john.doe@ivrtechnologym.com",
    "bill_to_company": "IVR Technology",
    "bill_to_address": "65 Lawrence Bell Drive",
    "bill_to_city": "Amherst",
    "bill_to_state": "New York",
    "bill_to_zip": "14221",
    "bill_to_country": "United States",
    "ship_to_first_name": "John",
    "ship_to_last_name": "Doe",
    "ship_to_email": "john.doe@ivrtechnology.com",
    "ship_to_company": "IVR Technology",
    "ship_to_address": "65 Lawrence Bell Drive",
    "ship_to_city": "Amherst",
    "ship_to_state": "New York",
    "ship_to_zip": "14221",
    "ship_to_country": "United States",
    "description": "Sale Testing",
    "invoice_id": "00000123"
}

Note: this is for testing and developing propuse.

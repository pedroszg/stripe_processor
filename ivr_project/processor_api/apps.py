from django.apps import AppConfig


class ProcessorApiConfig(AppConfig):
    name = 'processor_api'

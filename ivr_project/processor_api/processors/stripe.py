import re

from rest_framework import serializers
from ivr_project.chd_utils import get_values_to_mask
from .base import AbstractProcessor
from ivr_project.models import (
    RESULT_APPROVED,
    RESULT_ERROR,
    TRANSACTION_CURRENCY,
    ACH_OFFICIAL_COUNTRY,
)
from ..models import (
    PROCESSOR_STRIPE,
    ACH_ACCOUNT_TYPE_INDIVIDUAL,
    ACH_ACCOUNT_TYPE_COMPANY,
    TENDER_TYPE_CREDIT_CARD,
    TENDER_TYPE_ACH,
    TRANSACTION_TYPE_AUTHORIZE, TRANSACTION_TYPE_SALE, TRANSACTION_TYPE_VOID, TRANSACTION_TYPE_REFUND,
    TRANSACTION_TYPE_CAPTURE)
from ivr_project.utils import assign_if_not_none, validate_keys_in_dict
from ivr_project.requests_wrapper import Requests
import json
import codecs
import requests


class StripeProcessor(AbstractProcessor):
    name = PROCESSOR_STRIPE
    version = 'v1'
    payload = None
    api_access_key = None
    header = None
    source = None
    customer = None

    # a map between Strip's ach account types and our API's values
    api_ach_account_type_map = {
        ACH_ACCOUNT_TYPE_INDIVIDUAL: 'individual',
        ACH_ACCOUNT_TYPE_COMPANY: 'company',
    }

    # a map between Strip's response codes and our API's values
    api_response_code_map = {
        'succeeded': RESULT_APPROVED,
        'error': RESULT_ERROR,
    }
    # required credit card data
    api_require_card_fields = ['card_account_number', 'card_verification_value', 'card_expiry_month',
                               'card_expiry_year']

    def _validate_card_data(self):
        """ This function help to check that the credit card data have been sent into the client request """

        result = list()
        for data in self.api_require_card_fields:
            if not self.request.data[data]:
                result.append(data)
        return result

    def _validate_required_data(self):
        """ This function check that all required fields for the stripe transactions have been sent into the client
        request  """

        if self.transaction_request.transaction_type in (TRANSACTION_TYPE_SALE, TRANSACTION_TYPE_AUTHORIZE):

            if self.transaction_request.tender_type == TENDER_TYPE_ACH:

                if not self.transaction_request.ach_account_type:
                    raise serializers.ValidationError('account_type required for ach transaction.')

            if self.transaction_request.tender_type == TENDER_TYPE_CREDIT_CARD:

                if self.transaction_request.amount < 1:
                    raise serializers.ValidationError('For credit card transactions "amount" value cannot be '
                                                      'less than 1.')

                if self.transaction_request.card_verification_value:
                    try:
                        int(self.transaction_request.card_verification_value)
                    except ValueError:
                        raise serializers.ValidationError('card_verification_value must only consist of digits.')

                values = self._validate_card_data()
                if values:
                    raise serializers.ValidationError(
                        'One or more values are required ' + str(values) + ' for sale and authorize credit card transactions.')

        if self.transaction_request.transaction_type == TRANSACTION_TYPE_SALE:
            if not self.transaction_request.ach_account_number and \
                    self.transaction_request.tender_type == TENDER_TYPE_ACH:
                raise serializers.ValidationError('ach_account_number is required for "sale" ach transactions.')

        if self.transaction_request.transaction_type in (TRANSACTION_TYPE_CAPTURE, TRANSACTION_TYPE_VOID,
                                                         TRANSACTION_TYPE_REFUND) \
                and not self.transaction_request.original_transaction_id:
            raise serializers.ValidationError('original_transaction_id is required for "capture",'
                                              ' "void" and "refund" transactions.')


    def _validate_request(self):
        """ This control function helps to execute all validation functions for client request  """

        credentials = ['api_access_key']
        validate_keys_in_dict(credentials, self.transaction_request.credentials)

        self._validate_required_data()

    def _transaction_selector(self, transaction_type):
        """ This function selects url related to stripe transactions"""

        stripe_transactions_types = {
            "authorize": "/charges",
            "capture": "/charges/{}/capture",
            "sale": "/charges",
            "void": "/refunds",
            "refund": "/refunds"
        }

        return stripe_transactions_types[transaction_type]

    def _tokenize(self):
        """ This function generates a source token, it could be a credit card or bank account token"""

        self.payload = dict()
        url = "/tokens"

        if self.transaction_request.tender_type == TENDER_TYPE_CREDIT_CARD:
            self.payload['card[number]'] = self.transaction_request.card_account_number
            self.payload['card[exp_month]'] = int(self.transaction_request.card_expiry_month)
            self.payload['card[exp_year]'] = int(self.transaction_request.card_expiry_year)
            self.payload['card[cvc]'] = self.transaction_request.card_verification_value
        else:
            self.payload['bank_account[country]'] = ACH_OFFICIAL_COUNTRY
            self.payload['bank_account[currency]'] = TRANSACTION_CURRENCY
            self.payload['bank_account[account_holder_name]'] = self.transaction_request.ach_name_on_account
            self.payload['bank_account[account_holder_type]'] = self.api_ach_account_type_map
            [self.transaction_request.ach_account_type]
            self.payload['bank_account[routing_number]'] = self.transaction_request.ach_routing_number
            self.payload['bank_account[account_number]'] = self.transaction_request.ach_account_number

        self._process_request(url)

    def _bank_account_attach(self):
        """ This function helps to attach a source to a customer account"""

        self.payload = dict()
        url = '/customers'

        bank_token = self.api_response.json()['id']
        self.source = self.api_response.json()['bank_account']['id']
        self.payload['source'] = bank_token

        self._process_request(url)

    def _verify_bank_account(self):
        self.payload = dict()
        self.customer = self.api_response.json()['id']
        url = '/customers/{}/sources/{}/verify'.format(self.customer, self.source)

        self.payload['amounts[]'] = [32, 45]

        self._process_request(url)

    def _prepare_ach_payment(self):
        """ Control function that executes all previous transactions to complete ACH transaction successfully """

        self._tokenize()
        self._bank_account_attach()
        self._verify_bank_account()

    def _set_credentials(self):
        """ This method helps to set the api key, add "Bearer" authorization at the beginning of the api key"""

        self.api_access_key = 'Bearer ' + self.transaction_request.credentials['api_access_key']

    def _set_header(self):
        """ This method sets header authorization"""

        self.header = dict()
        self.header['Authorization'] = self.api_access_key

    def _set_endpoint(self):
        """ test or live mode is define by the api key, only one endpoint is required"""

        self.api_url = 'https://api.stripe.com/{}'.format(self.version)

    def _authorize(self):
        """Performs an authorize transaction."""

        self._tokenize()
        url = self._transaction_selector(self.transaction_request.transaction_type)

        self.payload['amount'] = self.transaction_request.amount_in_cents
        self.payload['currency'] = TRANSACTION_CURRENCY
        self.payload['source'] = self.api_response.json()['id']
        self.payload['capture'] = False
        self.payload['receipt_email'] = self.transaction_request.ship_to_email
        has_info, ship_to = self._payload_shipping_info()
        if has_info:
            self.payload.update(ship_to)
        self._process_request(url)

    def _capture(self):
        """Performs a capture transaction."""

        url = self._transaction_selector(self.transaction_request.transaction_type).format(
            self.transaction_request.original_transaction_id)

        self._process_request(url)

    def _sale(self):
        """Performs a sale transaction."""

        url = self._transaction_selector(self.transaction_request.transaction_type)

        if self.transaction_request.tender_type == TENDER_TYPE_CREDIT_CARD:
            self._tokenize()
            self.payload['source'] = self.api_response.json()['id']
            self.payload['receipt_email'] = self.transaction_request.ship_to_email
        else:
            self._prepare_ach_payment()
            self.payload['customer'] = self.customer
            self.payload['source'] = self.source
        self.payload['amount'] = self.transaction_request.amount_in_cents
        self.payload['currency'] = TRANSACTION_CURRENCY

        has_info, ship_to = self._payload_shipping_info()
        if has_info:
            self.payload.update(ship_to)

        self._process_request(url)

    def _void(self):
        """Performs a void transaction."""

        self.payload = dict()
        url = self._transaction_selector(self.transaction_request.transaction_type)

        self.payload["charge"] = self.transaction_request.original_transaction_id

        self._process_request(url)

    def _refund(self):
        """Performs a refund transaction."""

        self.payload = dict()
        url = self._transaction_selector(self.transaction_request.transaction_type)

        self.payload["charge"] = self.transaction_request.original_transaction_id

        self._process_request(url)

    def _parse_processor_response(self):
        if self.api_response.status_code == requests.codes.ok:
            try:
                response = self.api_response.json()
            except ValueError:
                response = json.loads(codecs.decode(self.api_response.content, 'utf-8-sig'))
            self.transaction_response.processor_response = response
            if response.get('status'):
                if response['status'] == 'pending':
                    response['status'] = 'succeeded'
                self.transaction_response.result = self.api_response_code_map[
                    response['status']]
                self.transaction_response.message = response['status'] + ' {}'.format(self.transaction_request.transaction_type)
                self.transaction_response.transaction_id = response.get('id', None)
        else:
            self.transaction_response.result = RESULT_ERROR
            self.transaction_response.message = 'HTTP {} - {}'.format(self.api_response.status_code,
                                                                      self.api_response.json()['error']['message'])

    def _payload_shipping_info(self):
        ship_to = dict()
        has_changed = False
        has_changed = assign_if_not_none(ship_to, 'shipping[name]',
                                         self.transaction_request.ship_to_first_name + ' ' +
                                         self.transaction_request.ship_to_last_name) or has_changed
        has_changed = assign_if_not_none(ship_to, 'shipping[address][line1]',
                                         self.transaction_request.ship_to_address) or has_changed
        has_changed = assign_if_not_none(ship_to, 'shipping[address][city]',
                                         self.transaction_request.ship_to_city) or has_changed
        has_changed = assign_if_not_none(ship_to, 'shipping[address][country]', ACH_OFFICIAL_COUNTRY) or has_changed
        has_changed = assign_if_not_none(ship_to, 'shipping[address][postal_code]',
                                         self.transaction_request.ship_to_zip) or has_changed
        has_changed = assign_if_not_none(ship_to, 'shipping[address][state]',
                                         self.transaction_request.ship_to_state) or has_changed
        return has_changed, ship_to

    def _process_request(self, transaction):
        """ This method sends the request to Stripe and gets the response. """

        self._set_header()
        r = Requests(self.transaction, to_mask=get_values_to_mask(self.request.data))
        self.api_response = r.send('POST', self.api_url + transaction, data=self.payload, headers=self.header)
        self.payload = dict()
